/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:34
**/

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
	{ path: "", redirectTo: "home", pathMatch: "full" },
	{ path: "about-us", loadChildren: "./pages/about-us/about-us.module#AboutUsPageModule" },
	{ path: "africa", loadChildren: "./pages/africa/africa.module#AfricaPageModule" },
	{ path: "asia-detail", loadChildren: "./pages/asia-detail/asia-detail.module#AsiaDetailPageModule" },
	{ path: "asia-detail/:post_id", loadChildren: "./pages/asia-detail/asia-detail.module#AsiaDetailPageModule" },
	{ path: "asia", loadChildren: "./pages/asia/asia.module#AsiaPageModule" },
	{ path: "asiavtex-detail", loadChildren: "./pages/asiavtex-detail/asiavtex-detail.module#AsiavtexDetailPageModule" },
	{ path: "asiavtex-detail/:post_id", loadChildren: "./pages/asiavtex-detail/asiavtex-detail.module#AsiavtexDetailPageModule" },
	{ path: "asiavtex", loadChildren: "./pages/asiavtex/asiavtex.module#AsiavtexPageModule" },
	{ path: "australia", loadChildren: "./pages/australia/australia.module#AustraliaPageModule" },
	{ path: "europe", loadChildren: "./pages/europe/europe.module#EuropePageModule" },
	{ path: "faqs", loadChildren: "./pages/faqs/faqs.module#FaqsPageModule" },
	{ path: "home", loadChildren: "./pages/home/home.module#HomePageModule" },
	{ path: "north-america", loadChildren: "./pages/north-america/north-america.module#NorthAmericaPageModule" },
	{ path: "privacy-policy", loadChildren: "./pages/privacy-policy/privacy-policy.module#PrivacyPolicyPageModule" },
	{ path: "south-america", loadChildren: "./pages/south-america/south-america.module#SouthAmericaPageModule" },
	{ path: "visathing", loadChildren: "./pages/visathing/visathing.module#VisathingPageModule" },
	{ path: "visathingexpress", loadChildren: "./pages/visathingexpress/visathingexpress.module#VisathingexpressPageModule" },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
