/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:34
**/

import { TestBed } from "@angular/core/testing";

import { AsiaService } from "./asia.service";

describe("AsiaService", () => {
	beforeEach(() => TestBed.configureTestingModule({}));
	
	it("should be created", () => {
		const service: AsiaService = TestBed.get(AsiaService);
		expect(service).toBeTruthy();
	});
});
