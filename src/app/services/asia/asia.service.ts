/**
 * @author nougheez <nafizulislam@gmail.com>
 * @copyright VISAThing 2019
 * @version 01.01.01
 * @license licenses.txt
 *
 * @date 2019-09-09 02:16:34
 **/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { retry } from 'rxjs/operators';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AsiaService {
  constructor(
    public httpClient: HttpClient,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController
  ) { }

  wpUrl: string = 'https://www.visathing.com';
  wpUrl2: string = 'https://www.visathingexpress.com';
  loading: any;

  /**
   * getPosts()
   **/

  getPosts(query): Observable<any> {
    this.presentLoading();
    let param = this.httpBuildQuery(query);
    return this.httpClient
      .get(this.wpUrl + `/wp-json/wp/v2/country/?${param}&_embed`)
      .pipe(
        map((results: any) => {
          console.log('RAW:', results);
          this.dismissLoading();
          this.showToast('Successfully!');

          return results;
        }),
        catchError(err => {
          console.log('throwError:', err);
          if (err.error.message) {
            this.showToast(err.error.message);
          } else {
            this.showAlert(err.statusText, err.name, err.message);
          }
          return throwError(err);
        }),
        catchError(err => {
          console.log('reThrown:', err);
          return of([]);
        })
      );
  }

  getPosts2(query): Observable<any> {
    this.presentLoading();
    let param = this.httpBuildQuery(query);
    return this.httpClient
      .get(this.wpUrl2 + `/wp-json/wp/v2/country/?${param}&_embed`)
      .pipe(
        map((results: any) => {
          // console.log('RAW:', results);
          //   this.dismissLoading();
          //   this.showToast('Successfully!');

          return results;
        }),
        catchError(err => {
          console.log('throwError:', err);
          if (err.error.message) {
            this.showToast(err.error.message);
          } else {
            this.showAlert(err.statusText, err.name, err.message);
          }
          return throwError(err);
        }),
        catchError(err => {
          console.log('reThrown:', err);
          return of([]);
        })
      );
  }

  /**
   * getPost()
   **/
  getPost(postId: string): Observable<any> {
    this.presentLoading();
    return this.httpClient
      .get(this.wpUrl + `/wp-json/wp/v2/country/${postId}?_embed=true`)
      .pipe(
        map(results => {
          console.log('RAW:', results);
          this.dismissLoading();
          this.showToast('Successfully!');
          return results;
        }),
        catchError(err => {
          console.log('throwError:', err);
          if (err.error.message) {
            this.showToast(err.error.message);
          } else {
            this.showAlert(err.statusText, err.name, err.message);
          }
          return throwError(err);
        }),
        catchError(err => {
          console.log('reThrown:', err);
          return of([]);
        })
      );
  }

  /**
   * httpBuildQuery(obj)
   * @param object $obj
   **/
  private httpBuildQuery(obj) {
    let k, str;
    str = [];
    for (k in obj) {
      str.push(encodeURIComponent(k) + '=' + encodeURIComponent(obj[k]));
    }
    return str.join('&');
  }

  /**
   * presentLoading()
   **/
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Please wait...',
      spinner: 'crescent',
      duration: 2000
    });
    await this.loading.present();
  }

  /**
   * dismissLoading()
   **/
  async dismissLoading() {
    if (this.loading) {
      await this.loading.dismiss();
    }
  }

  /**
   * showToast($message)
   **/
  async showToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      color: 'dark',
      duration: 500
    });
    await toast.present();
  }

  /**
   * showAlert()
   **/
  async showAlert(header: string, subheader: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subheader,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }
}
