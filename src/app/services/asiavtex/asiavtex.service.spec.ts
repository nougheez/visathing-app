/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:34
**/

import { TestBed } from "@angular/core/testing";

import { AsiavtexService } from "./asiavtex.service";

describe("AsiavtexService", () => {
	beforeEach(() => TestBed.configureTestingModule({}));
	
	it("should be created", () => {
		const service: AsiavtexService = TestBed.get(AsiavtexService);
		expect(service).toBeTruthy();
	});
});
