/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:34
**/

import { Component, OnInit } from "@angular/core";
import { PopoverController } from "@ionic/angular";
		

		
@Component({
	selector: "app-popover",
	templateUrl: "./popover.component.html",
	styleUrls: ["./popover.component.scss"],
})
		
		
export class PopoverComponent implements OnInit {
		
	/**
	* PopoverComponent:constructor()
	**/
	constructor(
		public popoverController: PopoverController
	){
		
		// constructor
		
	}
		
		
	ngOnInit() {
		
		// init
		
	}
		
		
	dismissPopover(){
		this.popoverController.dismiss();
	}

		
		
}
