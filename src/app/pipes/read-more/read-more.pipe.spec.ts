/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:34
**/

import { ReadMorePipe } from "./read-more.pipe";

describe("ReadMorePipe", () => {
	it("create an instance", () => {
		// please write the code manually
		const pipe = new ReadMorePipe();
		expect(pipe).toBeTruthy();
	});
});
