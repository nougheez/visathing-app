/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:34
**/

import { NgModule } from "@angular/core";

import { AppBrowserDirective } from "./app-browser/app-browser.directive";
import { AppWebviewDirective } from "./app-webview/app-webview.directive";
import { CallAppDirective } from "./call-app/call-app.directive";
import { FacebookAppDirective } from "./facebook-app/facebook-app.directive";
import { GeoAppDirective } from "./geo-app/geo-app.directive";
import { GooglePlayAppDirective } from "./google-play-app/google-play-app.directive";
import { LineAppDirective } from "./line-app/line-app.directive";
import { MailAppDirective } from "./mail-app/mail-app.directive";
import { SmsAppDirective } from "./sms-app/sms-app.directive";
import { SystemBrowserDirective } from "./system-browser/system-browser.directive";
import { TwitterAppDirective } from "./twitter-app/twitter-app.directive";
import { WhatsappAppDirective } from "./whatsapp-app/whatsapp-app.directive";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

@NgModule({
	declarations: [
		AppBrowserDirective,
		AppWebviewDirective,
		CallAppDirective,
		FacebookAppDirective,
		GeoAppDirective,
		GooglePlayAppDirective,
		LineAppDirective,
		MailAppDirective,
		SmsAppDirective,
		SystemBrowserDirective,
		TwitterAppDirective,
		WhatsappAppDirective
	],
	imports: [],
	exports: [
		AppBrowserDirective,
		AppWebviewDirective,
		CallAppDirective,
		FacebookAppDirective,
		GeoAppDirective,
		GooglePlayAppDirective,
		LineAppDirective,
		MailAppDirective,
		SmsAppDirective,
		SystemBrowserDirective,
		TwitterAppDirective,
		WhatsappAppDirective
	],
	providers: [
		InAppBrowser
	]
})

export class DirectivesModule {}
