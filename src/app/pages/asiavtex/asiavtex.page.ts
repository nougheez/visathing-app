/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:32
**/

import { Component , OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { AsiavtexService } from "./../../services/asiavtex/asiavtex.service";
import { PopoverController } from "@ionic/angular";
import { PopoverComponent } from "../../components/popover/popover.component";



@Component({
	selector: "app-asiavtex",
	templateUrl: "asiavtex.page.html",
	styleUrls: ["asiavtex.page.scss"],
})

export class AsiavtexPage {

	// search query
	filterQuery: string = "";

	/**
	* AsiavtexPage:constructor()
	**/
	constructor(
		private router: Router,
		public asiavtexService: AsiavtexService,
		public popoverController: PopoverController
	){
	

	}
	
	/**
	* AsiavtexPage:showPopover()
	**/
	async showPopover(ev: any) {
		const popover = await this.popoverController.create({
			component: PopoverComponent,
			event: ev,
			translucent: true
		});
		return await popover.present();
	}
	
	


		posts: Observable<any>;
	dataPosts: any = [];
	
	pageNumber: number = 1;
	catID: number = 9;
	query = {};
	
	/**
	* getPosts()
	**/
	getPosts(start: boolean){
		this.updateQuery();
		this.posts = this.asiavtexService.getPosts(this.query);
		this.posts.subscribe(data => {
			if(start == true){
				this.dataPosts = data ;
			}else{
				this.dataPosts = this.dataPosts.concat(data);
			}
		});
	}
	
	
	/**
	* loadMore(infiniteScroll)
	* @param event $infiniteScroll
	**/
	public loadMore(infiniteScroll){
		let pageNumber = this.pageNumber;
		this.pageNumber++;
		this.getPosts(false);
		setTimeout(() => {
			infiniteScroll.target.complete();
			//infiniteScroll.target.enable = false;
		}, 500);
	}
	
	
	/**
	* updateQuery()
	**/
	public updateQuery(){
		this.query["page"] = this.pageNumber;
		this.query["_embed"] = "true";
		this.query["per_page"] = 10 ;
		this.query["search"] = this.filterQuery;
		if(this.catID){
			this.query["forapp"] = this.catID;
		}
		console.log("parameter",this.query);
	}
	
	
	/**
	* doRefresh()
	**/
	public doRefresh(refresher){
		this.dataPosts = [];
		this.getPosts(false);
		setTimeout(() => {
			refresher.target.complete();
		}, 2000);
	}
	
	
	
	/**
	* filterItems($event)
	* @param any $event
	**/
	public filterItems(evt: any){
		let filterVal = evt.target.value;
		if (filterVal && filterVal.trim() !== "") {
			this.filterQuery = filterVal;
		}else{
			this.filterQuery = "";
		}
		if((this.filterQuery.length == 0 ) || (this.filterQuery.length >= 3 )){
			this.dataPosts = [];
			this.pageNumber = 1;
			this.getPosts(true);
		}
	}
	
	
	/**
	* ngOnInit()
	**/
	public ngOnInit(){
		this.dataPosts = [];
		this.pageNumber = 1;
		this.getPosts(true);
	}
	
	




}
