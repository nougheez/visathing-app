/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:33
**/

import { Component , OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PopoverController } from "@ionic/angular";
import { PopoverComponent } from "../../components/popover/popover.component";



@Component({
	selector: "app-south-america",
	templateUrl: "south-america.page.html",
	styleUrls: ["south-america.page.scss"],
})

export class SouthAmericaPage {
	/**
	* SouthAmericaPage:constructor()
	**/
	constructor(
		private router: Router,
		public popoverController: PopoverController
	){
	

	}
	
	/**
	* SouthAmericaPage:showPopover()
	**/
	async showPopover(ev: any) {
		const popover = await this.popoverController.create({
			component: PopoverComponent,
			event: ev,
			translucent: true
		});
		return await popover.present();
	}
	
	


	


	/**
	* SouthAmericaPage:ngOnInit()
	* @param string $url = 'http://ihsana.com/'
	**/
	public ngOnInit()
	{
	}  


}
