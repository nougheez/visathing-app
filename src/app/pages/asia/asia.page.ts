/**
 * @author nougheez <nafizulislam@gmail.com>
 * @copyright VISAThing 2019
 * @version 01.01.01
 * @license licenses.txt
 *
 * @date 2019-09-09 02:16:32
 **/

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AsiaService } from './../../services/asia/asia.service';
import { PopoverController } from '@ionic/angular';
import { PopoverComponent } from '../../components/popover/popover.component';

@Component({
  selector: 'app-asia',
  templateUrl: 'asia.page.html',
  styleUrls: ['asia.page.scss']
})
export class AsiaPage {

  /**
   * AsiaPage:constructor()
   **/
  constructor(
    private router: Router,
    public asiaService: AsiaService,
    public popoverController: PopoverController
  ) { }
  // search query
  filterQuery = '';

  posts: Observable<any>;
  dataPosts: any = [];

  catID = 670;
  continentID = 9;
  pageNumber = 1;
  query: any = {};

  /**
   * AsiaPage:showPopover()
   **/
  async showPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  /**
   * getPosts()
   **/
  getPosts(start: boolean) {
    this.updateQuery();
    this.posts = this.asiaService.getPosts(this.query);
    this.posts.subscribe((data: any) => {
      if (data.data && data.data.status === 400) {

      } else {

        if (start === true) {
          this.dataPosts = data;
        } else {
          this.dataPosts = this.dataPosts.concat(data);
        }
      }
    });
    const posts2 = this.asiaService.getPosts2(this.query)
      .subscribe((result: any) => {
        this.dataPosts = this.dataPosts.concat(result);
      })
  }

  /**
   * loadMore(infiniteScroll)
   * @param event $infiniteScroll
   **/
  public loadMore(infiniteScroll) {
    const pageNumber = this.pageNumber;
    this.pageNumber++;
    this.getPosts(false);
    setTimeout(() => {
      infiniteScroll.target.complete();
      // infiniteScroll.target.enable = false;
    }, 500);
  }

  /**
   * updateQuery()
   **/
  public updateQuery() {
    this.query.page = this.pageNumber;
    this.query._embed = 'true';
    this.query.per_page = 10;
    this.query.search = this.filterQuery;
    if (this.catID) {
      this.query.forapp = this.catID;
    }
    if (this.continentID) {
      this.query.continent = this.continentID;
    }
  }

  /**
   * doRefresh()
   **/
  public doRefresh(refresher) {
    this.dataPosts = [];
    this.getPosts(false);
    setTimeout(() => {
      refresher.target.complete();
    }, 2000);
  }

  /**
   * filterItems($event)
   * @param any $event
   **/
  public filterItems(evt: any) {
    const filterVal = evt.target.value;
    if (filterVal && filterVal.trim() !== '') {
      this.filterQuery = filterVal;
    } else {
      this.filterQuery = '';
    }
    if (this.filterQuery.length === 0 || this.filterQuery.length >= 3) {
      this.dataPosts = [];
      this.pageNumber = 1;
      this.getPosts(true);
    }
  }

  /**
   * ngOnInit()
   **/
  public ngOnInit() {
    this.dataPosts = [];
    this.pageNumber = 1;
    this.getPosts(true);
  }
}
