/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:32
**/

import { Component , OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PopoverController } from "@ionic/angular";
import { PopoverComponent } from "../../components/popover/popover.component";



@Component({
	selector: "app-home",
	templateUrl: "home.page.html",
	styleUrls: ["home.page.scss"],
})

export class HomePage {
	/**
	* HomePage:constructor()
	**/
	constructor(
		private router: Router,
		public popoverController: PopoverController
	){
	

	}
	
	/**
	* HomePage:showPopover()
	**/
	async showPopover(ev: any) {
		const popover = await this.popoverController.create({
			component: PopoverComponent,
			event: ev,
			translucent: true
		});
		return await popover.present();
	}
	
	


	slideItems: any;
public ngOnInit(){
	
	this.slideItems = 
[
    {
        "title": "Welcome to <br\/>VISAThing!",
        "description": "Your Visa Partner",
        "image": "assets\/images\/slides\/image-1.png"
    },
    {
        "title": "Like Magic!",
        "description": "Suspendisse rhoncus neque quis neque luctus, sit amet dictum ex condimentum",
        "image": "assets\/images\/slides\/image-2.png"
    },
    {
        "title": "Unlimited App!",
        "description": "Aliquam imperdiet pharetra ligula ut ullamcorper. Maecenas pharetra imperdiet nunc",
        "image": "assets\/images\/slides\/image-2.png"
    }
]
	
	}
	




}
