/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:32
**/

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { AsiavtexDetailPage } from "./asiavtex-detail.page";

describe("AsiavtexDetailPage", () => {
	let component: AsiavtexDetailPage;
	let fixture: ComponentFixture<AsiavtexDetailPage>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ AsiavtexDetailPage ],
			schemas: [CUSTOM_ELEMENTS_SCHEMA],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AsiavtexDetailPage);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
