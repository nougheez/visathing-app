/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:32
**/

import { Component , OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PopoverController } from "@ionic/angular";
import { PopoverComponent } from "../../components/popover/popover.component";



@Component({
	selector: "app-north-america",
	templateUrl: "north-america.page.html",
	styleUrls: ["north-america.page.scss"],
})

export class NorthAmericaPage {
	/**
	* NorthAmericaPage:constructor()
	**/
	constructor(
		private router: Router,
		public popoverController: PopoverController
	){
	

	}
	
	/**
	* NorthAmericaPage:showPopover()
	**/
	async showPopover(ev: any) {
		const popover = await this.popoverController.create({
			component: PopoverComponent,
			event: ev,
			translucent: true
		});
		return await popover.present();
	}
	
	


	


	/**
	* NorthAmericaPage:ngOnInit()
	* @param string $url = 'http://ihsana.com/'
	**/
	public ngOnInit()
	{
	}  


}
