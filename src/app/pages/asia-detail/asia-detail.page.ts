/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:32
**/

import { Component , OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { AsiaService } from "./../../services/asia/asia.service";
import { PopoverController } from "@ionic/angular";
import { PopoverComponent } from "../../components/popover/popover.component";



@Component({
	selector: "app-asia-detail",
	templateUrl: "asia-detail.page.html",
	styleUrls: ["asia-detail.page.scss"],
})

export class AsiaDetailPage {

	//url parameter
	public postId : string;

	/**
	* AsiaDetailPage:constructor()
	**/
	constructor(
		private router: Router,
		public asiaService: AsiaService,
		public popoverController: PopoverController,
		public activatedRoute: ActivatedRoute
	){
	
		this.postId = this.activatedRoute.snapshot.paramMap.get("post_id");

	}
	
	/**
	* AsiaDetailPage:showPopover()
	**/
	async showPopover(ev: any) {
		const popover = await this.popoverController.create({
			component: PopoverComponent,
			event: ev,
			translucent: true
		});
		return await popover.present();
	}
	
	


		post: Observable<any>;
	dataPost: any = {};
	
	/**
	* getPost(postId)
	**/
	public getPost(){
		this.post = this.asiaService.getPost(this.postId);
		this.post.subscribe(data => {
				this.dataPost = data ;
		});
	}
	
	
	/**
	* doRefresh()
	**/
	public doRefresh(refresher){
		this.dataPost = {};
		this.getPost();
		setTimeout(() => {
			refresher.target.complete();
		}, 2000);
	}
	
	
	/**
	* ngOnInit()
	**/
	public ngOnInit(){
		this.dataPost = {};
		this.getPost();
	}
	
	
	




}
