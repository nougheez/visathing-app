/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:33
**/

import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { VisathingexpressPage } from "./visathingexpress.page";

describe("VisathingexpressPage", () => {
	let component: VisathingexpressPage;
	let fixture: ComponentFixture<VisathingexpressPage>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ VisathingexpressPage ],
			schemas: [CUSTOM_ELEMENTS_SCHEMA],
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(VisathingexpressPage);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
