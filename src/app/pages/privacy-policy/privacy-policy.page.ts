/**
* @author nougheez <nafizulislam@gmail.com>
* @copyright VISAThing 2019
* @version 01.01.01
* @license licenses.txt
*
* @date 2019-09-09 02:16:32
**/

import { Component , OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PopoverController } from "@ionic/angular";
import { PopoverComponent } from "../../components/popover/popover.component";



@Component({
	selector: "app-privacy-policy",
	templateUrl: "privacy-policy.page.html",
	styleUrls: ["privacy-policy.page.scss"],
})

export class PrivacyPolicyPage {
	/**
	* PrivacyPolicyPage:constructor()
	**/
	constructor(
		private router: Router,
		public popoverController: PopoverController
	){
	

	}
	
	/**
	* PrivacyPolicyPage:showPopover()
	**/
	async showPopover(ev: any) {
		const popover = await this.popoverController.create({
			component: PopoverComponent,
			event: ev,
			translucent: true
		});
		return await popover.present();
	}
	
	


	


	/**
	* PrivacyPolicyPage:ngOnInit()
	* @param string $url = 'http://ihsana.com/'
	**/
	public ngOnInit()
	{
	}  


}
